package com.example.interview.service;

import com.example.interview.model.Post;
import com.google.gson.Gson;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.get;

public class PostCallingService {

    Gson gson = new Gson();

    public Post getPost(String url) {
        Response resp = get(url);
        String value = resp.asString();
        Post post = gson.fromJson(value, Post.class);
        return post;
    }
}
