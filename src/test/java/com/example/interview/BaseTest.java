package com.example.interview;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    WebDriver driver = new ChromeDriver();

    public void openWebPage(String url) {
        driver.navigate().to(url);
        driver.manage().window().maximize();
    }

    public void quitBrowser(){
        driver.quit();
    }

    public void sendKeys(By selector, String value) {
        driver.findElement(selector).sendKeys(value);
    }

    public void click(By selector) {
        driver.findElement(selector).click();
    }

    public void loginWebPage(String username, String password) {
        sendKeys(By.cssSelector("input#formHorizontalEmail"), username);
        sendKeys(By.cssSelector("input#formHorizontalPassword"), password);
        click(By.cssSelector(".col-login__btn"));
    }
}
