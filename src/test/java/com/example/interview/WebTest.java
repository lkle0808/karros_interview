package com.example.interview;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class WebTest extends BaseTest {
    String url = "http://ktvn-test.s3-website.us-east-1.amazonaws.com";
    String username = "admin@test.com";
    String password = "test123";

    @BeforeClass
    public void setup() {
        openWebPage(url);
    }

    @AfterClass
    public void cleanUp() {
        quitBrowser();
    }

    @Test
    public void countRequest() throws InterruptedException {
        loginWebPage(username, password);
        Thread.sleep(3000);

        //Get all rows in the table
        List<WebElement> tableRow = driver.findElements(By.xpath("(//table)[2]/tbody/tr"));
        int count = 0;
        for (WebElement rowText : tableRow) {
            String text = rowText.getText();
            if (text.contains("Approved") && text.contains("2019")) {
                count++;
            }

            //(1) get data from column 1 -> compare with 'Approved'
            //(2) get data from column 2 -> convert to date, get year == 2019
            // if(1 && 2) {
            //    count++;
            // }

        }
        System.out.println("Request count: " + count);
        Assert.assertEquals(5, count);
    }
}
