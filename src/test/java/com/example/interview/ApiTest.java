package com.example.interview;

import com.example.interview.model.Post;
import com.example.interview.service.PostCallingService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ApiTest {
    PostCallingService postCallingService = new PostCallingService();
    String url = "https://my-json-server.typicode.com/typicode/demo/posts/";

    public Post getById(int id) {
        return postCallingService.getPost(url + id);
    }

    @Test
    public void testPost1() {
        Post post = getById(1);
        Assert.assertEquals(post.getId(), 1);
        Assert.assertEquals(post.getTitle(), "Post 1");
    }

    @Test
    public void testPost2() {
        Post post = getById(2);
        Assert.assertEquals(post.getId(), 2);
        Assert.assertEquals(post.getTitle(), "Post 2");
    }
}
