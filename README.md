## Prerequisites

1. Java
2. Maven
3. Git

## How to run
**mvn clean test**

## Dependencies
1. selenium
2. restassured
3. testng
4. gson

---
## Improvements
1. Get value by column index instead of getting text in a row
2. Run with multiple browsers/platforms
3. Run with headless mode
4. Use constants for all Xpath
5. Move all configurable variables to config file (such as username, password, url...)
6. Use a different type of waiting (ImplicitWait, ExplicitWait, FluentWait...) instead of Thread.sleep

## Assumptions
1. Fixed column
2. Fixed data format
3. Only count on 1st page
